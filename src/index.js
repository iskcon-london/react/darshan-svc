import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import DarshanButton from './darshanDateButton';
import DarshanOutfitDetails from './darshanOutfitDetails';
import DarshanImage from './darshanImage';
import * as serviceWorker from './serviceWorker';

class Darshan extends React.Component {
	constructor(props) {
		super(props);
    this.state = {
      previous: {
        darshanDate: '11th',
        outfitDetails: 'Its a red!!!!',
        text: 'Previous',
        darshanImages: [
          {image: 'https://tafttest.com/100x60.jpg'},
          {image: 'https://tafttest.com/100x60.jpg'},
          {image: 'https://tafttest.com/100x60.jpg'},
          {image: 'https://tafttest.com/100x60.jpg'},
          {image: 'https://tafttest.com/100x60.jpg'},
        ],
      },
      latest: {
        darshanDate: '12th',
        outfitDetails: 'Its a pink!',
        text: 'Latest',
        darshanImages: [
          {image: 'http://www.placecage.com/100/60'},
          {image: 'http://www.placecage.com/100/60'},
          {image: 'http://www.placecage.com/100/60'},
          {image: 'http://www.placecage.com/100/60'},
          {image: 'http://www.placecage.com/100/60'},
        ],
      },
      selected: {
        darshanImages: [],
      },
    };
	}
	componentDidMount() {
    this.setState({ 
      selected: {
        ...this.state.latest,
      }
    });
    console.log(this.state);    
  }
	componentWillUnmount() {}
  previousDarshanButtonClick = () => {
    this.setState({ 
      selected: {
        ...this.state.previous,
      }
    });
  }

  latestDarshanButtonClick = () => {
    this.setState({ 
      selected: {
        ...this.state.latest,
      }
    });
  }
	render() {
    return (
      <div className='darshanComponent'>
        <div className='darshanComponent__dateButtons'>
          <DarshanButton text={this.state.previous.text} click={this.previousDarshanButtonClick} />
          <DarshanButton text={this.state.latest.text} click={this.latestDarshanButtonClick} />
        </div>
        <div className='darshanComponent__outfitDetails'>
          <DarshanOutfitDetails outfitDetails={this.state.selected.outfitDetails} darshanDate={this.state.selected.darshanDate} />
        </div>
        <div className='darshanComponent__darshanGallery'>
          {this.state.selected.darshanImages.map((object, key) =>
            <DarshanImage key={key} image={object.image} />
          )}
        </div>
      </div>
    )
  }
}

ReactDOM.render(<Darshan />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
