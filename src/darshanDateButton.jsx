import React from 'react';

function darshanDateButton(props) {
  return (
    <button onClick={props.click}>
      {props.text}
    </button>
  );
}

export default darshanDateButton;
